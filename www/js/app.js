// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic']);

app.config(function($stateProvider,$urlRouterProvider){

  $stateProvider
  .state('menu',{
    url: '/menu',
    abstract: true,
    templateUrl: 'menu.html',
    controller: 'menuCtrl'
  })
  .state('menu.cadastro',{
      url: '/cadastro',
      views: {'menuContent': { 
        templateUrl: 'cadastro.html',
        controller: 'cadastroCtrl'
      }}    
  })
  .state('menu.listar',{
      url: '/listar',
      views: {'menuContent': {
      templateUrl: 'listar.html',
      controller: 'cadastroCtrl'
      }}
  });
  $urlRouterProvider.otherwise('/menu/cadastro'); 
});

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
  });
});

app.controller('menuCtrl',function($scope,$ionicSideMenuDelegate){
  
  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };  

});

app.controller('cadastroCtrl',function($scope,$ionicPopup){

    $scope.data = {};

    $scope.loadAll = function(){

      pessoas = [];

      keys = Object.keys(window.localStorage);

      for (var i =0 ; i < window.localStorage.length; i++){
          pessoas.push(JSON.parse(window.localStorage.getItem(keys[i])));
      }
      $scope.pessoas = pessoas;
    }  

    $scope.adicionar = function(){
      if (angular.isDefined($scope.data.nome)){
        window.localStorage.setItem($scope.data.nome,angular.toJson({nome: $scope.data.nome}));
        $scope.loadAll();
  
        $ionicPopup.alert({
          title: "Cadastro",
          template: "Salvo com sucesso!!"
        });
  
      }
      $scope.data.nome=undefined;

    }

    $scope.remover = function(index){
      keys = Object.keys(window.localStorage);
      window.localStorage.removeItem(keys[index]);
      pessoas.splice(keys[index],1);
      $scope.loadAll(); 

      $ionicPopup.alert({
        title: "Listar",
        template: "Excluído com sucesso!!"
      });

    }

    $scope.loadAll();

});